<?php get_header(); ?>

<div id="body">
	<div class="container">
		<div class="row">
			<div id="main" class="col-md-12">
				<h1><?php _e('Błąd 404', 'aloa'); ?></h1>
				<p><?php _e('Nie znaleziono strony', 'aloa'); ?></p>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
