# aloa

  - gulp
  - sass
  - wordpress
  - bootstrap
  - slick slider
  - blazy
  - matchheight
  - scrollreveal

kube ~> taco ~> **aloa**

### Version
1.0.0

## Usage
### GIT
```sh
$ git clone https://jatne@bitbucket.org/jatne/aloasource.git
```

### Install
```sh
$ npm install
```

### Tasks
#### Styles
```sh
$ gulp styles
```
#### Vendors JS
```sh
$ gulp vendorsJS
```
#### custom JS
```sh
$ gulp customJS
```
#### minify images
```sh
$ gulp images
```
#### create pot file (translate)
```sh
$ gulp translate
```
#### build
styles+vendorsJS+customJS+images+translate
```sh
$ gulp build
```
#### buildFiles
build+copy files to dist folder called as project
```sh
$ gulp buildFiles
```
#### default task
styles+vendorsJS+customJS+images+browsersync
```sh
$ gulp
```

### Gulp plugins
- browser-sync
- gulp
- gulp-autoprefixer
- gulp-concat
- gulp-filter
- gulp-imagemin
- gulp-line-ending-corrector
- gulp-merge-media-queries
- gulp-notify
- gulp-rename
- gulp-sass
- gulp-sourcemaps
- gulp-uglify
- gulp-uglifycss
- gulp-sort
- gulp-wp-pot

### Todos

 - Build zip archive

License
----
MIT