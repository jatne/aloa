WebFontConfig = {
	classes: false,
	google : { families: [ 'FONT:WEIGHT:latin,latin-ext' ] },
	active : function() { sessionStorage.fonts = true; }
};

(function($) {
	var wf   = document.createElement('script');
	wf.src   = '//ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
	wf.type  = 'text/javascript';
	wf.async = 'true';
	var s    = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(wf, s);

	$(document).ready(function() {
		var templateUrl = object_name.templateUrl;
		window.sr = ScrollReveal({
			duration: 1000,
			scale   : 1,
			mobile  : false,
			easing  : 'ease'
		});
	});

	$(window).load(function() {});
})(jQuery);
