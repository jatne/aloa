        <footer id="footer">
            <div class="container">
                <div class="row">
                    <?php dynamic_sidebar('footer'); ?>
                </div>
            </div>
        </footer>
    <?php wp_footer(); ?>
    </body>
</html>
