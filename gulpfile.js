/**
* Gulpfile
*
* @author Malok
* @version 2.0
*/

/**
* Configuration
*/
var project    = 'aloa',
    projectUrl = 'faber.dev/',
    productURL = './',
    build      = './'+ project +'/';

//translation
var text_domain    = 'XX',
    destFile       = 'xx_XX.pot',
    packageName    = 'XX',
    bugReport      = 'http://weblider.eu',
    lastTranslator = 'Paweł Malok <pawel.malok@weblider.eu>',
    team           = 'weblider <pawel.malok@weblider.eu>',
    translatePath  = './languages';

//style
var styleSRC         = './assets/css/style.scss',
    styleDestination = './';

//js vendor
var jsVendorSRC         = './assets/js/vendor/*.js',
    jsVendorDestination = './assets/js/',
    jsVendorFile        = 'vendors';

//js custom
var jsCustomSRC         = './assets/js/custom/*.js',
    jsCustomDestination = './assets/js/',
    jsCustomFile        = 'custom';

//img
var imagesSRC         = './assets/img/raw/**/*.{png,jpg,gif,svg}',
    imagesDestination = './assets/img/';

//watch files path
var styleWatchFiles      = '/assets/css/**/*.scss',
    vendorJSWatchFiles   = '/assets/js/vendor/*.js',
    customJSWatchFiles   = '/assets/js/custom/*.js',
    projectPHPWatchFiles = '/**/*.php';

var autoprefixerOptions = {
  browsers: [
    'last 2 versions',
    '> 1%',
    'ie >= 9',
    'ff > 40',
    'ie_mob >= 10',
    'android >= 4'
  ]
};

var buildFilesPath = [
    // common file types
    '**/*.php',
    '**/*.html',
    '**/*.css',
    '**/*.js',

    // fonts
    '**/*.ttf',
    '**/*.otf',
    '**/*.eot',
    '**/*.woff',
    '**/*.woff2',

    // translate
    '**/*.pot',
    '**/*.mo',
    '**/*.po',

    // video
    '**/*.mov',
    '**/*.mp4',
    '**/*.webm',

    //img
    '**/*.svg',
    '**/*.ico',
    '**/*.jpg',
    '**/*.png',
    '**/*.gif',

    // screenshot
    'screenshot.png',

    // exclude
    '!node_modules/**/*',
    '!style.css.map',
    '!gulpfile.js',
    '!assets/js/custom/*',
    '!assets/js/vendor/*',
    '!assets/js/_plugins/*',
    '!assets/img/raw/*',
];


/**
* Load Plugins
*/
var gulp = require('gulp');

//css
var sass         = require('gulp-sass'),
    minifycss    = require('gulp-uglifycss'),
    autoprefixer = require('gulp-autoprefixer');

//js
var concat = require('gulp-concat'),
    uglify = require('gulp-uglify');

//img
var imagemin = require('gulp-imagemin');

//utility
var rename      = require('gulp-rename'),
    lineec      = require('gulp-line-ending-corrector'),
    filter      = require('gulp-filter'),
    sourcemaps  = require('gulp-sourcemaps'),
    sort        = require('gulp-sort'),
    wpPot       = require('gulp-wp-pot'),
    browserSync = require('browser-sync').create(),
    reload      = browserSync.reload;

/**
* Tasks
*/
// gulp browser-sync
gulp.task('browser-sync', function() {
    browserSync.init({
        proxy        : projectUrl,
        open         : false,
        injectChanges: true,
        online       : true,
        // port         : 7000
    });
});


//gulp styles
gulp.task('styles', function() {
    gulp.src( styleSRC )
        .pipe( sourcemaps.init() )
        .pipe( sass( {
            errLogToConsole: true,
            outputStyle    : 'compact',
            // outputStyle    : 'compressed',
            // outputStyle    : 'nested',
            // outputStyle    : 'expanded',
            precision      : 10
        } ) )
        .on('error', console.log.bind(console))
        .pipe( sourcemaps.write( { includeContent: false } ) )
        .pipe( sourcemaps.init( { loadMaps: true } ) )
        .pipe( autoprefixer( autoprefixerOptions ) )
        .pipe( sourcemaps.write( styleDestination ) )
        .pipe( lineec() )
        .pipe( gulp.dest( styleDestination ) )
        .pipe( filter( '**/*.css' ) )
        .pipe( browserSync.stream() )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( minifycss( { maxLineLen: 0 } ) )
        .pipe( lineec() )
        .pipe( gulp.dest( styleDestination ) )
        .pipe( filter( '**/*.css' ) )
        .pipe( browserSync.stream() );
});

//gulp vendorsJS
gulp.task('vendorsJS', function() {
    gulp.src( jsVendorSRC )
        .pipe( concat( jsVendorFile + '.js' ) )
        .pipe( lineec() )
        .pipe( gulp.dest( jsVendorDestination ) )
        .pipe( rename( {
            basename: jsVendorFile,
            suffix: '.min'
        } ) )
        .pipe( uglify() )
        .pipe( lineec() )
        .pipe( gulp.dest( jsVendorDestination ) );
});

// gulp customJS
gulp.task('customJS', function() {
    gulp.src( jsCustomSRC )
        .pipe( concat( jsCustomFile + '.js' ) )
        .pipe( lineec() )
        .pipe( gulp.dest( jsCustomDestination ) )
        .pipe( rename( {
            basename: jsCustomFile,
            suffix: '.min'
        } ) )
        .pipe( uglify() )
        .pipe( lineec() )
        .pipe( gulp.dest( jsCustomDestination ) );
});

//gulp images
// gulp.task('images', function() {
//     gulp.src( imagesSRC )
//         .pipe( imagemin( {
//             progressive: true,
//             optimizationLevel: 3,
//             interlaced: true,
//             svgoPlugins: [{removeViewBox: false}]
//         } ) )
//         .pipe( gulp.dest( imagesDestination ) );
// });

//gulp translate
// gulp.task( 'translate', function() {
//     return gulp.src( projectPHPWatchFiles )
//         .pipe(sort())
//         .pipe(wpPot( {
//             domain        : text_domain,
//             destFile      : destFile,
//             package       : packageName,
//             bugReport     : bugReport,
//             lastTranslator: lastTranslator,
//             team          : team
//         } ))
//         .pipe(gulp.dest(translatePath));
// });

// gulp build
gulp.task('build', ['styles', 'vendorsJS', 'customJS'], function() {
    console.log('Zadanie "build" zakończone');
});

// gulp buildFiles
gulp.task('buildFiles', ['build'], function() {
    return 	gulp.src(buildFilesPath)
        .pipe(gulp.dest(build));
});

gulp.task('watch', ['styles', 'vendorsJS', 'customJS', 'browser-sync'], function() {
	gulp.watch( projectPHPWatchFiles, reload );
	gulp.watch( styleWatchFiles, [ 'styles' ] );
	gulp.watch( vendorJSWatchFiles, [ 'vendorsJS', reload ] );
	gulp.watch( customJSWatchFiles, [ 'customJS', reload ] );
});

gulp.task('watch-only', ['styles', 'vendorsJS', 'customJS'], function() {
    gulp.watch( projectPHPWatchFiles, reload );
    gulp.watch( styleWatchFiles, [ 'styles' ] );
    gulp.watch( vendorJSWatchFiles, [ 'vendorsJS', reload ] );
    gulp.watch( customJSWatchFiles, [ 'customJS', reload ] );
});

gulp.task( 'default', ['build', 'browser-sync'], function() {
	gulp.watch( projectPHPWatchFiles, reload );
	gulp.watch( styleWatchFiles, [ 'styles' ] );
	gulp.watch( vendorJSWatchFiles, [ 'vendorsJS', reload ] );
	gulp.watch( customJSWatchFiles, [ 'customJS', reload ] );
});
