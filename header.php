<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo tempDir(); ?>/assets/img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="application-name" content="<?php bloginfo('name'); ?>"/>

    <title><?php wp_title('|', true, 'right'); ?></title>

    <!-- <link rel="dns-prefetch" href="//fonts.googleapis.com"> -->
    <!-- <link rel="dns-prefetch" href="//www.google-analytics.com"> -->

    <link rel="shortcut icon" type="image/x-icon" href="<?php echo tempDir(); ?>/assets/img/favicon/favicon.ico" />
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo tempDir(); ?>/assets/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo tempDir(); ?>/assets/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo tempDir(); ?>/assets/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo tempDir(); ?>/assets/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo tempDir(); ?>/assets/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo tempDir(); ?>/assets/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo tempDir(); ?>/assets/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo tempDir(); ?>/assets/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo tempDir(); ?>/assets/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo tempDir(); ?>/assets/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo tempDir(); ?>/assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo tempDir(); ?>/assets/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo tempDir(); ?>/assets/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo tempDir(); ?>/assets/img/favicon/manifest.json">

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <header id="header">
        <nav class="navbar" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle  collapsed" data-toggle="collapse" data-target="#navtop">
                        <span class="sr-only"><?php _e('Rozwiń menu', 'aloa'); ?></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="<?php echo home_url(); ?>" rel="home" class="navbar-brand">
                        <img src="<?php echo tempDir(); ?>/assets/img/logo@2.png" alt="<?php bloginfo('name'); ?>" width="151" height="65" />
                    </a>
                </div><!-- /.navbar-header -->
                <div id="navtop" class="collapse navbar-collapse">
                    <?php
                        wp_nav_menu( array(
                            'menu'           => 'primary-navigation',
                            'theme_location' => 'primary-navigation',
                            'depth'          => 2,
                            'menu_class'     => 'nav navbar-nav',
                            'fallback_cb'    => 'wp_bootstrap_navwalker::fallback',
                            'walker'         => new wp_bootstrap_navwalker())
                        );
                    ?>
                    <form class="navbar-form navbar-right hidden-sm" method="GET" action="<?php bloginfo('url'); ?>" role="search">
                        <div class="form-wrapper">
                            <div class="form-group">
                                <div class="col-xs-9">
                                    <input type="search" name="s" value="<?php echo get_search_query(); ?>"  class="form-control" placeholder="<?php _e('Szukaj...', 'aloa'); ?>" required>
                                </div>
                                <div class="col-xs-3">
                                    <button type="submit" class="btn btn-default"><?php _e('Szukaj', 'aloa'); ?></button>
                                </div>
                            </div>
                        </div>
                    </form><!-- /form.navbar-form -->
                </div><!-- /#navtop -->
            </div><!-- /.container -->
        </nav><!-- /nav.navbar -->
    </header>
