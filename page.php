<?php get_header(); ?>

<div id="body">
	<div class="container">

		<div class="row">
			<div id="main" class="col-md-12">
				<?php
				while( have_posts() ) {
					the_post();
					the_content();
				}
				?>
			</div>
		</div>

	</div>
</div>

<?php get_footer(); ?>
