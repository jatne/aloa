<aside id="sidebar" class="col-md-4">
	<?php if( !dynamic_sidebar('sidebar') ) : ?>
		<div class="widget">
			<h4 class="widget__header"><?php _e('Brak widgetów', 'aloa'); ?></h4>
			<p><?php _e('Widgety można dodawać w panelu administracyjnym', 'aloa'); ?></p>
		</div>
	<?php endif; ?>
</aside>
