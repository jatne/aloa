<?php get_header(); ?>


<div id="body"><div class="container">

	<div class="row">
		<div id="main" class="col-md-8">

			<?php while( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class() ?>>

					<header class="post__header">
						<h1 class="post__header__title">
							<?php the_title(); ?>
						</h1>

						<div class="post__header__date">
							<?php _e('Data', 'aloa'); ?>:
							<time datetime="<?php echo get_the_date('c') ?>">
								<?php the_time(get_option('date_format')); ?>
							</time>
						</div>

						<?php if( comments_open() ) : ?>
						<div class="post__header__comments">
							<?php comments_popup_link(__('Brak komentarzy', 'aloa'), __('Jeden komentarz', 'aloa'), __('Komentarzy: %', 'aloa')); ?>
						</div>
						<?php endif; ?>

						<div class="post__header__author">
							<?php echo get_avatar(get_the_author_meta('ID'), 36); ?></span>
							<?php _e('Autor', 'aloa'); ?>: <?php the_author_posts_link(); ?>
						</div>

						<div class="post__header__categories">
							<?php _e('Kategoria', 'aloa'); ?>:
							<?php the_category(', '); ?>
						</div>

						<?php the_tags('<div class="post__header__tags">'.__('Tagi', 'aloa').': ', ', ', '</div>'); ?>
					</header>

					<div class="post__body">
						<?php
						if(has_post_thumbnail())
							the_post_thumbnail( array(960,240), array('alt'=>$post->post_name, 'class'=>'post__content__thumbnail') );
						?>
						<div class="post__body__content"><?php the_content(); ?></div>
					</div>

					<footer class="post__footer">
						<ul class="pagination">
							<li><?php next_post_link('%link', '&laquo; '.__('Nowszy wpis', 'aloa'), true ); ?></li>
							<li style="float:right;"><?php previous_post_link('%link', __('Starszy wpis', 'aloa').' &raquo;', true ); ?></li>
						</ul>
					</footer>

					<?php comments_template(); ?>

				</article>

			<?php endwhile; ?>

		</div><!-- end of #main -->

		<?php get_sidebar(); ?>

	</div><!-- end .units-row -->

</div></div>


<?php get_footer(); ?>
